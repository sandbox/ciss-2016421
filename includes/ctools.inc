<?php

function ys_usage_ctools_page_detail($type, $name)
{
  $plugins = ctools_get_plugins('ctools', $type);
  if(!isset($plugins[$name]))
    return 'Plugin not found.';

  $resources = array();

  switch($type)
  {
    case 'access':
      // Retrieve panes using the access plugin (panels_pane.access)
      #$resources['pane'] = ys_usage_ctools_detail_access_in_panes($name);
      // Retrieve variants using the access plugin (page_manager_handlers.conf)
      #$resources['variant'] = ys_usage_ctools_detail_access_in_variants($name);
      // Retrieve pages using the access plugin (page_manager_pages.access)
      $resources['page'] = ys_usage_ctools_detail_access_in_pages($name);

//      $like_string = '%%s:7:"plugins";%%' . $name_string . '%%';
//      $results = db_query('SELECT pmp.pid, pmp.name FROM page_manager_pages pmp WHERE pmp.access LIKE "%s"', $like_string);
//      while($row = db_fetch_object($results))
//      {
//
//      }
      break;

    case 'arguments':
      // Retrieve pages using the argument plugin (page_manager_pages.arguments)
      $resources['page'] = ys_usage_ctools_detail_argument_in_pages($name);
      break;

    case 'contexts':
      // Retrieve variants using the context plugin (page_manager_handlers.conf)
      $resources['variant'] = ys_usage_ctools_detail_context_in_variants($name);
      // Retrieve mini panels using the context plugin (panels_mini.requiredcontexts + panels_mini.contexts)
      $resources['panels_mini'] = ys_usage_ctools_detail_context_in_mini_panels($name);
      break;

    case 'relationships':
      // Retrieve variants using the relationship plugin (page_manager_handlers.conf)
      $resources['variant'] = ys_usage_ctools_detail_relationship_in_variants($name);
      // Retrieve mini panels using relationship plugin (panels_mini.relationships)
      $resourcs['panels_mini'] = ys_usage_ctools_detail_relationship_in_mini_panels($name);
      break;

    case 'content_types':
      // Retrieve panes using the content type plugin (panels_pane.type, page_manager_handlers.did)
      $resources['variant'] = ys_usage_ctools_detail_content_type_in_variants($name);
      // Retrieve mini panels using the content type plugin (panels_pane.type, panels_mini.did)
      $resources['panels_mini'] = ys_usage_ctools_detail_content_type_in_mini_panels($name);
      break;
  }
  $resources = array_filter($resources);
  $output = '';
  ys_usage_table_init();
  drupal_set_title(sprintf('%s | %s (%s)', $type, $name, $plugins[$name]['module']));
  foreach($resources as $resource_type => $elements)
    $output .= theme("ys_usage_ctools_table_{$resource_type}_{$type}", $elements);
  if(empty($resources))
    $output .= '<p>No resources seem to be using this plugin.</p>';
  return $output;
}


function ys_usage_ctools_detail_access_in_pages($name)
{
  $resources = array();
  $status_pages = variable_get('default_page_manager_pages', array());
  $like_string = '%' . serialize('name') . serialize($name) . '%';
  $query = '
    SELECT pmp.name, pmp.admin_title, pmp.access
    FROM page_manager_pages pmp
    WHERE pmp.access LIKE "%s"
  ';
  $results = db_query($query, $like_string);
  while($row = db_fetch_object($results))
  {
    $access_settings = unserialize($row->access);
    if(empty($access_settings['plugins']))
      continue;
    foreach($access_settings['plugins'] as $access)
    {
      dd($access, 'ACCESS');
      if($access['name'] === $name)
        $resources[] = array(
          'page' => array(
            'name' => $row->name,
            'title' => $row->admin_title,
            'status' => empty($status_pages[$name])
          ),
          'access' => array(
            'settings' => isset($access['settings']) ? $access['settings'] : array(),
            'context' => isset($access['context']) ? $access['context'] : null
          ),
        );
    }
  }
  return $resources;

}


function ys_usage_ctools_detail_relationship_in_variants($name)
{
  $resources = array();
  $status_handlers = variable_get('default_page_manager_handlers', array());
  $status_pages = variable_get('default_page_manager_pages', array());
  $like_string = '%' . serialize('relationships') . '%' . serialize('name') . serialize($name) . '%';
  $query = '
    SELECT pmh.name, pmh.conf, pmh.subtask, pmp.admin_title
    FROM page_manager_handlers pmh
    LEFT JOIN page_manager_pages pmp ON pmh.subtask = pmp.name
    WHERE pmh.conf LIKE "%s"
    ';
  $results = db_query($query, $like_string);
  while($row = db_fetch_object($results))
  {
    $conf = unserialize($row->conf);
    if(empty($conf['relationships']))
      continue;
    foreach($conf['relationships'] as $relationship)
      if($relationship['name'] === $name)
      {
        $resources[] = array(
          'handler' => array(
            'name'  => $row->name,
            'title' => $conf['title'],
            'status' => empty($status_handlers[$row->name])
          ),
          'page'  => array(
            'name'  => $row->subtask,
            'title' => $row->admin_title,
            'status' => empty($status_pages[$row->subtask])
          ),
          'relationship' => array(
            'keyword' => $relationship['keyword'],
            'settings' => isset($relationship['relationship_settings']) ? $relationship['relationship_settings'] : null,
          ),
        );
      }
  }
  return $resources;
}


function ys_usage_ctools_detail_relationship_in_mini_panels($name)
{
  $resources = array();
  $status_mini_panels = variable_get('default_panels_mini', array());
  $like_string = '%' . serialize('name') . serialize($name) . '%';
  $query = '
        SELECT pm.name, pm.admin_title, pm.relationships
        FROM panels_mini pm
        WHERE pm.relationships LIKE "%s"
        ';
  $results = db_query($query, $like_string, $like_string);
  while($row = db_fetch_object($results))
  {
      foreach($row->relationships as $relationship)
        if($relationship['name'] === $name)
        {
          $resources[] = array(
            'panels_mini' => array(
              'name'  => $row->name,
              'title' => $row->admin_title,
              'status' => empty($status_mini_panels[$row->name])
            ),
            'context' => array(
              'keyword' => $relationship['keyword'],
              'settings' => isset($relationship['relationship_settings']) ? $relationship['relationship_settings'] : null,
            ),
          );
        }
  }
  return $resources;
}



function ys_usage_ctools_detail_content_type_in_variants($name)
{
  $resources = array();
  $status_handlers = variable_get('default_page_manager_handlers', array());
  $status_pages = variable_get('default_page_manager_pages', array());
  $pane_query = '
    SELECT pp.did, pp.shown, pp.configuration, pp.position
    FROM panels_pane pp
    WHERE pp.type = "%s"
  ';
  $pane_results = db_query($pane_query, $name);
  $query = '
      SELECT
        pmh.name handler_name, pmh.conf handler_conf,
        pmp.name page_name, pmp.admin_title page_title
      FROM
        page_manager_handlers pmh
        LEFT JOIN page_manager_pages pmp ON pmh.subtask = pmp.name
      WHERE
        pmh.conf LIKE "%s"
    ';
  while($row = db_fetch_array($pane_results))
  {
    $like_string = '%' . serialize('did') . serialize((string) $row['did']) . '%';
    if(!$handler = db_fetch_array(db_query($query, $like_string)))
      continue;
    $row = (object) ($row + $handler);
    $conf = unserialize($row->handler_conf);
    $resources[] = array(
      'page' => array(
        'name' => $row->page_name,
        'title' => $row->page_title,
        'status' => empty($status_pages[$row->page_name])
      ),
      'handler' => array(
        'name' => $row->handler_name,
        'title' => $conf['title'],
        'status' => empty($status_handlers[$row->handler_name])
      ),
      'content_type' => array(
      'status' => !empty($row->shown),
      'settings' => unserialize($row->configuration)
      )
    );
  }
  return $resources;
}

function ys_usage_ctools_detail_content_type_in_mini_panels($name)
{
  $resources = array();
  $status_mini_panels = variable_get('default_panels_mini', array());
  $query = '
    SELECT pp.shown, pp.configuration, pp.position, pm.name, pm.admin_title
    FROM panels_pane pp INNER JOIN panels_mini pm ON pp.did = pm.did
    WHERE pp.type = "%s"
  ';
  $results = db_query($query, $name);
  while($row = db_fetch_object($results))
    $resources[] = array(
      'panels_mini' => array(
        'name' => $row->name,
        'title' => $row->admin_title,
        'status' => empty($status_mini_panels[$row->name])
      ),
      'content_type' => array(
        'status' => !empty($row->shown),
        'settings' => unserialize($row->configuration)
      )
    );
  return $resources;
}


function ys_usage_ctools_detail_argument_in_pages($name)
{
  $resources = array();
  $status_pages = variable_get('default_page_manager_pages', array());
  $like_string = '%' . serialize('name') . serialize($name) . '%';
  $query = '
    SELECT pmp.name, pmp.admin_title, pmp.arguments
    FROM page_manager_pages pmp
    WHERE pmp.arguments LIKE "%s"
  ';
  $results = db_query($query, $like_string);
  while($row = db_fetch_object($results))
  {
    $arguments = unserialize($row->arguments);
    foreach($arguments as $key => $argument)
    {
      if($argument['name'] === $name)
        $resources[] = array(
          'page' => array(
            'name' => $row->name,
            'title' => $row->admin_title,
            'status' => empty($status_pages[$name])
          ),
          'argument' => array(
            'identifier' => $argument['identifier'],
            'settings' => isset($argument['settings']) ? $argument['settings'] : array()
          ),
        );
    }
  }
  return $resources;
}


/**
 * Retrieve panels variants using the given context plugin
 */
function ys_usage_ctools_detail_context_in_variants($name)
{
  $resources = array();
  $status_handlers = variable_get('default_page_manager_handlers', array());
  $status_pages = variable_get('default_page_manager_pages', array());
  $like_string = '%' . serialize('contexts') . '%' . serialize('name') . serialize($name) . '%';
  $query = '
        SELECT pmh.name, pmh.conf, pmh.subtask, pmp.admin_title
        FROM page_manager_handlers pmh
        LEFT JOIN page_manager_pages pmp ON pmh.subtask = pmp.name
        WHERE pmh.conf LIKE "%s"
        ';
  $results = db_query($query, $like_string);
  while($row = db_fetch_object($results))
  {
    $conf = unserialize($row->conf);
    if(empty($conf['contexts']))
      continue;
    foreach($conf['contexts'] as $context)
      if($context['name'] === $name)
      {
        $resources[] = array(
          'handler' => array(
            'name'  => $row->name,
            'title' => $conf['title'],
            'status' => empty($status_handlers[$row->name])
          ),
          'page'  => array(
            'name'  => $row->subtask,
            'title' => $row->admin_title,
            'status' => empty($status_pages[$row->subtask])
          ),
          'context' => array(
            'keyword' => $context['keyword'],
            'settings' => isset($context['context_settings']) ? $context['context_settings'] : null,
          ),
        );
      }
  }
  return $resources;
}


/**
 * Retrieve mini panels using the given context plugin
 */
function ys_usage_ctools_detail_context_in_mini_panels($name)
{
  $resources = array();
  $status_mini_panels = variable_get('default_panels_mini', array());
  $like_string = '%' . serialize('name') . serialize($name) . '%';
  $query = '
        SELECT pm.name, pm.admin_title, pm.requiredcontexts, pm.contexts
        FROM panels_mini pm
        WHERE pm.requiredcontexts LIKE "%s" OR pm.contexts LIKE "%s"
        ';
  $results = db_query($query, $like_string, $like_string);
  while($row = db_fetch_object($results))
  {
    $context_types = array(
      'required' => unserialize($row->requiredcontexts),
      'optional' => unserialize($row->contexts)
    );
    foreach($context_types as $context_type => $contexts)
      foreach($contexts as $context)
        if($context['name'] === $name)
        {
          $resources[] = array(
            'panels_mini' => array(
              'name'  => $row->name,
              'title' => $row->admin_title,
              'status' => empty($status_mini_panels[$row->name])
            ),
            'context' => array(
              'keyword' => $context['keyword'],
              'settings' => isset($context['context_settings']) ? $context['context_settings'] : null,
              'type' => $context_type
            ),
          );
        }
  }
  return $resources;
}


function ys_usage_ctools_page_summary($form_state)
{
  $values = isset($form_state['storage']) ? $form_state['storage'] : array();
  $plugin_types = array('arguments', 'contexts', 'content_types', 'relationships', 'access');
  $plugins = array();
  foreach($plugin_types as $type)
    $plugins[$type] = ctools_get_plugins('ctools', $type);

  $headers = array('Plugin type', 'Module', 'Name', 'Description', '');
  $rows = array();
  foreach($plugins as $type => $type_plugins)
  {
    if(!empty($values) && !isset($values['type'][$type]))
      continue;
    foreach($type_plugins as $name => $plugin)
    {
      if(!empty($values) && !isset($values['module'][$plugin['module']]))
        continue;

      $row = array(
        $type,
        $plugin['module'],
        $name,
        $plugin['title'] . (isset($plugin['description']) ? '<br /><em>' . $plugin['description'] . '</em>' : ''),
        l('Details', "admin/ys_usage/ctools/detail/$type/$name")
      );

      if(!empty($values['sort']))
      {
        $sort_keys = array(
          'type' => "$type:$plugin[module]:$name",
          'module' => "$plugin[module]:$type:$name",
          'name' => "$name:$plugin[module]:$type"
        );
        $rows[$sort_keys[$values['sort']]] = $row;
      }
      else
        $rows[] = $row;
    }
  }
  if(!empty($values['sort']))
    ksort($rows);

  $types = array_keys(array_filter($plugins));
  $types = array_combine($types, $types);
  $modules = array();
  foreach($plugins as $type => $type_plugins)
    foreach($type_plugins as $name => $plugin)
      $modules[$plugin['module']] = $plugin['module'];
  asort($modules);

  $list = theme('table', $headers, $rows);

  $form = array(
    '#attributes' => array('class' => 'ys-usage-filter-form')
  );
  $form['style'] = array(
    '#type' => 'markup',
    '#value' => '
      <style type="text/css">
      form.ys-usage-filter-form .form-item {float:left;padding-right:10px}
      form.ys-usage-filter-form .form-submit {margin-top:25px}
      </style>
    '
  );
  $form['type'] = array(
    '#type' => 'select',
    '#title' => 'Plugin type',
    '#size' => 5,
    '#multiple' => true,
    '#options' => $types,
    '#default_value' => isset($values['type']) ? $values['type'] : array_keys($types),
  );
  $form['module'] = array(
    '#type' => 'select',
    '#title' => 'Module',
    '#size' => 5,
    '#multiple' => true,
    '#options' => $modules,
    '#default_value' => isset($values['module']) ? $values['module'] : array_keys($modules),
  );
  $form['sort'] = array(
    '#title' => 'Sort by',
    '#type' => 'select',
    '#size' => 5,
    '#options' => array(
      '' => '',
      'type' => 'Plugin type',
      'module' => 'Module',
      'name' => 'Name'
    ),
    '#default_value' => isset($values['sort']) ? $values['sort'] : '',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Filter',
  );
  $form['list'] = array('#type' => 'markup', '#value' => $list);
  return $form;
}


function ys_usage_ctools_page_summary_submit($form, &$form_state)
{
  $form_state['storage'] = $form_state['values'];
}

