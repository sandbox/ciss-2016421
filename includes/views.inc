<?php

/**
 * Page callback; provides a list of views linked to their usage details.
 *
 * @return mixed
 */
function ys_usage_views_page_summary()
{
  $views = views_get_all_views();
  $list = array();
  $view_names = array_keys($views);
  sort($view_names);
  foreach($view_names as $view)
    $list[] = l($view, "admin/ys_usage/views/$view");
  return theme('item_list', $list, 'Views');
}


/**
 * Page callback; provides detailed usage information for a specific view.
 *
 * @param $view_name
 * @return string
 */
function ys_usage_views_page_detail($view_name)
{
  $usage = array();
  $view = views_get_view($view_name);
  foreach($view->display as $display => $options)
  {
    $usage[$display] = array('page' => array(), 'mini_panel' => array(), 'block' => array(), 'context' => array(), 'view' => array());
    $usage[$display]['page'] += _ys_usage_views_get_panels_page(_ys_usage_views_get_panels_pane_did($view_name, $display, 'views'));
    $usage[$display]['mini_panel'] += _ys_usage_views_get_panels_mini(_ys_usage_views_get_panels_pane_did($view_name, $display, 'views'));

    if(module_exists('views_field_view'))
      $usage[$display]['view'] += _ys_usage_views_get_view($view_name, $display);

    switch($options->display_plugin)
    {
      case 'panel_pane':
        $usage[$display]['page'] += _ys_usage_views_get_panels_page(_ys_usage_views_get_panels_pane_did($view_name, $display, 'views_panes'));
        $usage[$display]['mini_panel'] += _ys_usage_views_get_panels_mini(_ys_usage_views_get_panels_pane_did($view_name, $display, 'views_panes'));
        break;

      case 'block':
        $usage[$display]['block'] += _ys_usage_views_get_block($view_name, $display, 'core');
        $usage[$display]['context'] += _ys_usage_views_get_block($view_name, $display, 'context');
        break;
    }
  }

  $output = '';
  $output .= '<h1>' . l($view_name, "admin/build/views/edit/$view_name") . '</h1>';
  $table_data = array();

  foreach($usage as $display => $usages)
  {
    $display_link = l($display, "admin/build/views/edit/$view_name", array('fragment' => "views-tab-$display"));
    $usages = array_filter($usages);
    foreach($usages as $res_type => $resources)
      foreach($resources as $res)
      {
        $link = is_array($res['link'])
          ? l($res['title'], $res['link'][0], $res['link'][1])
          : l($res['title'], $res['link']);
        $table_data[] = array($display_link, $res_type, $link, $res['status'] ? t('enabled') : t('disabled'));

      }
  }
  $output .= theme('table', array('Display', 'Resource type', 'Resource name', 'Resource status'), $table_data);
  return $output;
}


/**
 * Returns all pages implementing any of the provided display ids.
 *
 * @param $did_list
 * @return array
 */
function _ys_usage_views_get_panels_page($did_list)
{
  $pages = array();
  if(!$did_list)
    return $pages;

  $status_handlers = variable_get('default_page_manager_handlers', array());
  $status_pages = variable_get('default_page_manager_pages', array());

  $display_ids = array();
  foreach($did_list as $did)
    $display_ids[$did] = sprintf('s:3:"did";s:%d:"%d";', strlen($did), $did);

  $query = '
		SELECT pmh.*, pmp.admin_title
		FROM page_manager_handlers pmh
		LEFT JOIN page_manager_pages pmp ON pmh.subtask = pmp.name
		WHERE pmh.conf REGEXP \'%s\'

	';
  $result = db_query($query, implode('|', $display_ids));
  while($row = db_fetch_object($result))
  {
    $handler_conf = unserialize($row->conf);
    $title = $handler_conf['title'];
    if(!empty($row->subtask))
    {
      $subtask = $row->subtask;
      $subtask_op = "page-$subtask";

    }
    else
    {
      // Try to derive the subtask name
      list($subtask) = explode('_panel_context', $row->name, 2);
      $subtask_op = $subtask;
    }
    $link = "admin/build/pages/nojs/operation/$subtask_op/handlers/$row->name/content";
    $pages["$subtask:$row->name"] = array(
      'title' => !empty($row->admin_title) ? "$row->admin_title: $title" : $title,
      'link' => $link,
      'status' => !isset($status_handlers[$row->name]) && !isset($status_pages[$subtask])
    );
  }
  asort($pages);
  return $pages;
}


/**
 * Returns all mini panels implementing any of the provided display ids.
 *
 * @param $did_list
 * @return array
 */
function _ys_usage_views_get_panels_mini($did_list)
{
  $mini_panels = array();
  $status = variable_get('default_panels_mini', array());
  if(!$did_list)
    return $mini_panels;
  $result = db_query('SELECT * FROM panels_mini pm WHERE pm.did IN (' . db_placeholders($did_list) . ')', $did_list);
  while($row = db_fetch_object($result))
    $mini_panels[$row->name] = array(
      'title' => $row->admin_title,
      'link' => "admin/build/mini-panels/list/$row->name/edit/content",
      'status' => empty($status[$row->name])
    );

  return $mini_panels;
}


/**
 * Returns all blocks for the given view.
 * For $type == 'core': Return all enabled blocks.
 * For $type == 'context': Return all blocks that are part of a context module context.
 *
 * @param $view
 * @param $display
 * @param $type
 * @return array
 */
function _ys_usage_views_get_block($view, $display, $type)
{
  $delta = "$view-$display";
  if(strlen($delta) >= 32)
    $delta = md5($delta);

  switch($type)
  {
    case 'core':
      return _ys_usage_blocks_get_block($delta, 'views');

    case 'context':
      return _ys_usage_blocks_get_context($delta);

    default:
      return array();
  }
}


/**
 * Get all enabled blocks for a particular $delta (and $module)
 *
 * @param string $delta
 * @param string $module
 * @return array
 */
function _ys_usage_blocks_get_block($delta, $module = null)
{
  $blocks = array();
  $args = array($delta);
  $cond_module = '';
  if($module)
  {
    $cond_module = ' AND b.module = "%s"';
    $args[] = $module;
  }
  $query = 'SELECT * FROM blocks b WHERE b.status = 1 AND b.delta = "%s"' . $cond_module;
  $result = db_query($query, $args);
  while($row = db_fetch_object($result))
    $blocks["$row->delta:$row->theme"] = array(
      'title' => "$row->module:$row->delta ($row->theme)",
      'link' => "admin/build/block/configure/$row->module/$row->delta",
      'status' => true
    );

  return $blocks;
}

/**
 * Return all context module contexts containing the block $delta
 *
 * @param string $delta
 * @return array
 */
function _ys_usage_blocks_get_context($delta)
{
  $contexts = array();
  $context_status = variable_get('context_status', array());
  $query = 'SELECT * FROM context c WHERE c.reactions LIKE \'%%s:5:"block";a:1:{s:6:"blocks";a:"%%:"%s"\'';
  $result = db_query($query, $delta);
  while($row = db_fetch_object($result))
    $contexts[$row->name] = array(
      'title' => $row->name,
      'link' => "admin/build/context/list/$row->name/edit",
      'status' => !empty($context_status[$row->name])
    );

  return $contexts;
}


/**
 * Returns all views implementing the given view:display via views_field_view.
 *
 * @param $view
 * @param $display
 * @return array
 */
function _ys_usage_views_get_view($view, $display)
{
  # %%;s:5:"table";s:5:"views";s:5:"field";s:4:"view";%%
  # %%;s:%d:"view";s:12:"%s";s:7:"display";s:%d:"%s";%%
  $views = array();
  $query = '
		SELECT vv.name, vv.human_name, vd.id, vd.display_title, vd.display_options
		FROM views_view vv
		INNER JOIN views_display vd ON vv.vid = vd.vid
		WHERE
			vd.display_options LIKE \'%%;s:5:"table";s:5:"views";s:5:"field";s:4:"view";%%\'
			AND vd.display_options LIKE \'%%;s:4:"view";s:%d:"%s";s:7:"display";s:%d:"%s";%%\'
	';
  $result = db_query($query, strlen($view), $view, strlen($display), $display);
  while($row = db_fetch_object($result))
  {
    $view_title = strlen($row->human_name) ? $row->human_name : $row->name;
    $display_title = strlen($row->display_title) ? $row->display_title : $row->id;
    $display_options = unserialize($row->display_options);
    $views["$row->name:$row->id"] = array(
      'title' => "$view_title ($display_title)",
      'link' => array("admin/build/views/edit/$row->name", array('fragment' => "views-tab-$row->id")),
      'status' => (!isset($display_options['enabled']) || $display_options['enabled']),
    );
  }
  return $views;
}



/**
 * Return panels display IDs for a particular view display.
 *
 * @param $view string
 * @param $display string
 * @param $type string     One of "views", "views_panes"
 * @return array
 */
function _ys_usage_views_get_panels_pane_did($view, $display, $type)
{
  $dids = array();
  switch($type)
  {
    case 'views':
      $query = '
				SELECT *
				FROM panels_pane pp
				WHERE (pp.type = "views" AND pp.subtype = "%s" AND pp.configuration LIKE \'%%s:7:"display";s:%d:"%s";%%\')
			';
      $result = db_query($query, $view, strlen($display), $display);
      break;

    case 'views_panes':
      $result = db_query('SELECT * FROM panels_pane pp WHERE pp.type = "views_panes" AND pp.subtype = "%s-%s"', $view, $display);
      break;
  }
  if(!empty($result))
    while($row = db_fetch_object($result))
      $dids[$row->did] = $row->did;

  return $dids;
}
