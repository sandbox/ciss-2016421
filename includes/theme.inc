<?php

function ys_usage_theme()
{
  $resources = array('arguments' => array('resources' => array()));
  $items = array();
  $items['ys_usage_panels_settings'] = array(
    'arguments' => array(
      'data' => array()
    ),
  );
  $items['ys_usage_ctools_table_variant_contexts'] = $resources;
  $items['ys_usage_ctools_table_panels_mini_contexts'] = $resources;

  $items['ys_usage_ctools_table_page_arguments'] = $resources;

  $items['ys_usage_ctools_table_variant_content_types'] = $resources;
  $items['ys_usage_ctools_table_panels_mini_content_types'] = $resources;

  $items['ys_usage_ctools_table_variant_relationships'] = $resources;
  $items['ys_usage_ctools_table_panels_mini_relationships'] = $resources;

  $items['ys_usage_ctools_table_page_access'] = $resources;
  $items['ys_usage_ctools_table_variant_access'] = $resources;
  $items['ys_usage_ctools_table_pane_access'] = $resources;

  return $items;
}

function ys_usage_table_init()
{
  static $init = false;
  if(!$init)
  {
    $path = drupal_get_path('module', 'ys_usage');
    drupal_add_js($path . '/scripts/tablesorter/jquery.tablesorter.min.js');
    drupal_add_js($path . '/ys_usage.js');
    $init = true;
  }
}


function theme_ys_usage_panels_settings($data)
{
  $items = array();
  foreach((array) $data as $key => $value)
  {
    if(is_bool($value))
      $value = $value ? 'TRUE' : 'FALSE';
    elseif(is_null($value))
      $value = 'NULL';
    $value = is_array($value) ? theme('ys_usage_panels_settings', $value) : check_plain($value);

    $items[] = sprintf('<strong>%s</strong>: <span>%s</span>', check_plain($key), $value);
  }
  return theme('item_list', $items);
}


function theme_ys_usage_ctools_table_page_arguments($resources)
{
  $headers = array('Page', 'Identifier', 'Settings', '');
  $rows = array();
  foreach($resources as $resource)
  {
    $page = $resource['page'];
    $argument = $resource['argument'];
    $rows[] = array(
      $page['title'] . '<br /><em>' . $page['name'] . '</em>',
      $argument['identifier'],
      theme('ys_usage_panels_settings', $argument['settings']),
      l('Edit', 'admin/build/pages/nojs/operation/page-' . $page['name'] . '/settings/argument')
    );
  }
  return theme('table', $headers, $rows, array('class' => 'tablesorter'), 'Panels pages');
}


function theme_ys_usage_ctools_table_variant_contexts($resources)
{
  $headers = array('Page', 'Variant', 'Keyword', 'Settings', '');
  $rows = array();
  foreach($resources as $resource)
  {
    $page = $resource['page'];
    $handler = $resource['handler'];
    $context = $resource['context'];
    $rows[] = array(
      $page['title'] . '<br /><em>' . $page['name'] . '</em>',
      $handler['title'],
      $context['keyword'],
      theme('ys_usage_panels_settings', $context['settings']),
      l('Edit', 'admin/build/pages/nojs/operation/page-' . $page['name'] . '/handlers/' . $handler['name'] . '/context')
    );
  }
  return theme('table', $headers, $rows, array('class' => 'tablesorter'), 'Panels variants');
}


function theme_ys_usage_ctools_table_panels_mini_contexts($resources)
{
  $headers = array('Mini panel', 'Keyword', 'Type', 'Settings', '');
  $rows = array();
  foreach($resources as $resource)
  {
    $mini_panel = $resource['panels_mini'];
    $context = $resource['context'];
    $rows[] = array(
      $mini_panel['title'] . '<br /><em>' . $mini_panel['name'] . '</em>',
      $context['keyword'],
      $context['type'],
      theme('ys_usage_panels_settings', $context['settings']),
      l('Edit', 'admin/build/mini-panels/list/' . $mini_panel['name'] . '/edit/context'),
    );
  }
  return theme('table', $headers, $rows, array('class' => 'tablesorter'), 'Mini panels');
}


function theme_ys_usage_ctools_table_variant_content_types($resources)
{
  $headers = array('Page', 'Variant', 'Settings', '');
  $rows = array();
  foreach($resources as $resource)
  {
    $page = $resource['page'];
    $handler = $resource['handler'];
    $content_type = $resource['content_type'];
    $rows[] = array(
      $page['title'] . '<br /><em>' . $page['name'] . '</em>',
      $handler['title'],
      theme('ys_usage_panels_settings', $content_type['settings']),
      l('Edit', 'admin/build/pages/nojs/operation/page-' . $page['name'] . '/handlers/' . $handler['name'] . '/content')
    );
  }
  return theme('table', $headers, $rows, array('class' => 'tablesorter'), 'Panels variants');
}


function theme_ys_usage_ctools_table_panels_mini_content_types($resources)
{
  $headers = array('Name', 'Settings', '');
  $rows = array();
  foreach($resources as $resource)
  {
    $mini_panel = $resource['panels_mini'];
    $content_type = $resource['content_type'];
    $rows[] = array(
      $mini_panel['title'] . '<br /><em>' . $mini_panel['name'] . '</em>',
      theme('ys_usage_panels_settings', $content_type['settings']),
      l('Edit', 'admin/build/mini-panels/list/' . $mini_panel['name'] . '/edit/context'),
    );
  }
  return theme('table', $headers, $rows, array('class' => 'tablesorter'), 'Mini panels');
}


function theme_ys_usage_ctools_table_variant_relationships($resources)
{
  $headers = array('Page', 'Variant', 'Keyword', 'Settings', '');
  $rows = array();
  foreach($resources as $resource)
  {
    $page = $resource['page'];
    $handler = $resource['handler'];
    $relationship = $resource['relationship'];
    $rows[] = array(
      $page['title'] . '<br /><em>' . $page['name'] . '</em>',
      $handler['title'],
      $relationship['keyword'],
      theme('ys_usage_panels_settings', $relationship['settings']),
      l('Edit', 'admin/build/pages/nojs/operation/page-' . $page['name'] . '/handlers/' . $handler['name'] . '/context'),
    );
  }
  return theme('table', $headers, $rows, array('class' => 'tablesorter'), 'Panels variants');
}


function theme_ys_usage_ctools_table_panels_mini_relationships($resources)
{
  $headers = array('Mini panel', 'Keyword', 'Settings', '');
  $rows = array();
  foreach($resources as $resource)
  {
    $mini_panel = $resource['panels_mini'];
    $relationship = $resource['relationship'];
    $rows[] = array(
      $mini_panel['title'] . '<br /><em>' . $mini_panel['name'] . '</em>',
      theme('ys_usage_panels_settings', $relationship['settings']),
      l('Edit', 'admin/build/mini-panels/list/' . $mini_panel['name'] . '/edit/context')
    );
  }
  return theme('table', $headers, $rows, array('class' => 'tablesorter'), 'Mini panels');
}


function theme_ys_usage_ctools_table_page_access($resources)
{
  $headers = array('Page', 'Settings', 'Context', '');
  $rows = array();
  foreach($resources as $resource)
  {
    $page = $resource['page'];
    $access = $resource['access'];
    $rows[] = array(
      $page['title'] . '<br /><em>' . $page['name'] . '</em>',
      theme('ys_usage_panels_settings', $access['settings']),
      $access['context'],
      l('Edit', 'admin/build/pages/nojs/operation/page-' . $page['name'] . '/settings/access')
    );
  }
  return theme('table', $headers, $rows, array('class' => 'tablesorter'), 'Panels pages');
}
