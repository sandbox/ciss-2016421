Drupal.behaviors.ysUsageTablesort = function(context) {
    $(context).find('table.tablesorter').tablesorter()
    .bind('sortEnd', function() {
        var rows = $(this).find('tr');
        rows.filter(':odd').removeClass('even').addClass('odd');
        rows.filter(':even').removeClass('odd').addClass('even');
    });
}